import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CopyCatBehaviour implements IPlayerBehaviour, Listener{
    private String opponentMove;
    public CopyCatBehaviour() {
        this.opponentMove = MoveType.COOPERATE.value;
    }
    public String makeMove() {
        return this.opponentMove;
    }

    public void receive(String msg) {
        String[] moves = msg.split(",");
        List<String> movesList = new ArrayList<>(Arrays.asList(moves));
        movesList.remove(this.opponentMove);
        if(movesList.size() >= 1) {
            this.opponentMove = movesList.get(0);
        }
    }
 }
