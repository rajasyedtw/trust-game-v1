public class CheatBehaviour implements IPlayerBehaviour {

    @Override
    public String makeMove() {
        return MoveType.CHEAT.value;
    }
}
