public interface Listener {
    void receive(String msg);
}
