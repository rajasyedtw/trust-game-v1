import java.util.Scanner;

public class Player {

    private Scanner input;
    private int score;
    private IPlayerBehaviour playerBehaviour;

    public Player(Scanner sc)
    {
        this.input = sc;
    }

    public Player(IPlayerBehaviour playerBehaviour) {
        this.playerBehaviour = playerBehaviour;
    }

    public int finalScore() {
        return score;
    }

    public String makeMove() {

        return playerBehaviour.makeMove();
    }
    public void updateScore(int score){
        this.score += score;
    }
}
