import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game {

    private List<Listener> listenerList;
    private Player player1;
    private Player player2;
    private Machine machine;
    public Game(Player player1, Player player2, Machine machine) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.listenerList = new ArrayList<>();
    }

    public void play(int rounds) {
        for(int i = 0; i < rounds ; i++) {
            String player1Move = player1.makeMove();
            String player2Move = player2.makeMove();
            Score score = machine.compute(player1Move, player2Move);
            player1.updateScore(score.getPlayer1());
            player2.updateScore(score.getPlayer2());
            System.out.println("Player1 : " + score.getPlayer1() + " , Player2 : " + score.getPlayer2());

            notifyListeners(player1Move + "," + player2Move);

        }
        System.out.println("Final Score : Player1 : " + player1.finalScore() +" Player2: " + player2.finalScore());
    }

    public void addListner(Listener listener) {
        listenerList.add(listener);
    }

    public void notifyListeners(String msg) {
        for(Listener listener : listenerList) {
            listener.receive(msg);
        }
    }
}
