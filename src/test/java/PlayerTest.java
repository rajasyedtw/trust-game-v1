import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class PlayerTest {

    @Test
    public void shouldReturnCooperateMoveforPlayer(){
        ConsoleBehaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        String moveType = player.makeMove();
        assertEquals(MoveType.COOPERATE.value, moveType);
    }

    @Test
    public void shouldReturnCheatMoveforPlayer(){
        ConsoleBehaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("2"));
        Player player = new Player(consoleBehaviour);
        String moveType = player.makeMove();
        assertEquals(MoveType.CHEAT.value, moveType);
    }

    @Test
    public void shouldReturnBadMoveforInvalid(){
        ConsoleBehaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("5"));
        Player player = new Player(consoleBehaviour);
        String moveType = player.makeMove();
        assertEquals( null, moveType);
    }

    @Test
    public void shouldUpdateTheScore(){
        ConsoleBehaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        player.updateScore(2);
        assertEquals(2,player.finalScore());

    }

    @Test
    public void shouldReturnCooperateMoveOnlyforCooperatePlayer(){
        CooperateBehaviour cooperateBehaviour = new CooperateBehaviour();
        Player player = new Player(cooperateBehaviour);
        String moveType = player.makeMove();
        assertEquals(MoveType.COOPERATE.value, moveType);
    }

    @Test
    public void shouldReturnCheatMoveOnlyforCheatPlayer(){
        CheatBehaviour cheatBehaviour = new CheatBehaviour();
        Player player = new Player(cheatBehaviour);
        String moveType = player.makeMove();
        assertEquals(MoveType.CHEAT.value, moveType);
    }
}
