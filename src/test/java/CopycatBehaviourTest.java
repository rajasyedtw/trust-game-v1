import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CopycatBehaviourTest {

    @Test
    public void shouldReturnOnlyCooperateMoveforFirstTime(){
        CopyCatBehaviour copycatBehaviour = new CopyCatBehaviour();
        String moveType = copycatBehaviour.makeMove();
        assertEquals(MoveType.COOPERATE.value, moveType);
    }

}
