import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class GameTestIT {

    @Test
    public void shouldVerifyScoreFor2Rounds(){
        ConsoleBehaviour consoleBehaviour1 = new ConsoleBehaviour(new Scanner("1 \n 2"));
        ConsoleBehaviour consoleBehaviour2 = new ConsoleBehaviour(new Scanner("1 \n 1"));

        Player player1 = new Player(consoleBehaviour1);
        Player player2 = new Player(consoleBehaviour2);

        Machine machine = new Machine();

        Game game = new Game(player1, player2, machine);

        game.play(2);

        assertEquals(5,player1.finalScore());
        assertEquals(1,player2.finalScore());
    }

    @Test
    public void testGameBetweenCoolAndCheat(){
        IPlayerBehaviour cooperateBehaviour = new CooperateBehaviour();
        IPlayerBehaviour cheatBehaviour = new CheatBehaviour();

        Player player1 = new Player(cooperateBehaviour);
        Player player2 = new Player(cheatBehaviour);

        Machine machine = new Machine();

        Game game = new Game(player1, player2, machine);

        game.play(5);

        assertEquals(-5,player1.finalScore());
        assertEquals(15,player2.finalScore());
    }

    @Test
    public void testGameBetweenCopycatAndOther(){
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        ConsoleBehaviour consoleBehaviour2 = new ConsoleBehaviour(new Scanner("1 \n 2 \n 1"));

        Player player1 = new Player(copyCatBehaviour);

        Player player2 = new Player(consoleBehaviour2);

        Machine machine = new Machine();

        Game game = new Game(player2, player1, machine);
        game.addListner(copyCatBehaviour);

        game.play(3);

        assertEquals(4,player1.finalScore());
        assertEquals(4,player2.finalScore());
    }

    @Test
    public void testGameBetweenCopycatAndCool(){
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        IPlayerBehaviour cooperateBehaviour = new CooperateBehaviour();

        Player player1 = new Player(copyCatBehaviour);

        Player player2 = new Player(cooperateBehaviour);

        Machine machine = new Machine();

        Game game = new Game(player1, player2, machine);
        game.addListner(copyCatBehaviour);

        game.play(3);

        assertEquals(6,player1.finalScore());
        assertEquals(6,player2.finalScore());
    }

    @Test
    public void testGameBetweenCopycatAndCheat(){
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        IPlayerBehaviour cheatBehaviour = new CheatBehaviour();

        Player player1 = new Player(copyCatBehaviour);

        Player player2 = new Player(cheatBehaviour);

        Machine machine = new Machine();

        Game game = new Game(player1, player2, machine);
        game.addListner(copyCatBehaviour);

        game.play(3);

        assertEquals(-1,player1.finalScore());
        assertEquals(3,player2.finalScore());
    }

    @Test
    public void testGameBetweenCopyCatAndCopycat(){
        CopyCatBehaviour copyCatBehaviour1 = new CopyCatBehaviour();
        CopyCatBehaviour copyCatBehaviour2 = new CopyCatBehaviour();

        Player player1 = new Player(copyCatBehaviour1);

        Player player2 = new Player(copyCatBehaviour2);

        Machine machine = new Machine();

        Game game = new Game(player2, player1, machine);
        game.addListner(copyCatBehaviour1);
        game.addListner(copyCatBehaviour2);

        game.play(3);

        assertEquals(6,player1.finalScore());
        assertEquals(6,player2.finalScore());
    }


}
